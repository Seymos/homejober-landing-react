import React from "react";
import "./style.scss"
import BetaForm from "../Forms/beta-form";

const Modal = ({ closeModal }) => {
	return (
		<div className="modal is-active">
			<div className="modal-background"></div>
			<div className="modal-content">
				<h2 className='is-medium'>Inscription à la bêta fermée</h2>
				<p className='is-medium'>
					Une bêta fermée de l'application est prévue courant octobre 2019. L'inscription ne vous engage à rien et vous permet d'accéder à l'outil en avant première !
				</p>
				<p>
					Nous dévoilerons les fonctionnalités de Homejober au fur et à mesure alors inscrivez-vous avec votre e-mail pour ne rien rater !
				</p>
				<BetaForm />
			</div>
			<button className="modal-close is-large" aria-label="close" onClick={closeModal}></button>
		</div>
	)
}

export default Modal