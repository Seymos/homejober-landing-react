import React, { Component } from "react"
import "./beta-form.scss"
import {stringify} from "query-string";

class BetaForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			accept: false,
			message: '',
			alert_color: ''
		}
	}

	handleSubmit = (event) => {
		event.preventDefault()
		if(this.validateEmail(this.state.email) === true) {
			console.log('email valid')
		} else {
			console.log('email invalid')
		}
		const { email } = { ...this.state }
		fetch(``, {
			method: 'POST',
			body: stringify({email})
		})
			.then(response => {
				return response.json()
			})
			.then(data => {
				console.log(data)
			})
		this.setState({
			email: '',
			accept: false
		})
	}

	handleChange = (event) => {
		const { name, value } = event.target
		this.setState({
			[name]: value
		})
	}

	handleAccept = () => {
		this.setState({ accept: !this.state.accept })
	}

	validateEmail = (email) => {
		const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}

	render() {
		const { email, accept, message, alert_color } = { ...this.state }
		return (
			<form method='POST' onSubmit={this.handleSubmit}>
				<div className="field">
					<label className="label">E-mail</label>
					<div className="control has-icons-left has-icons-right">
						<input className={`input is-medium ${alert_color}`} type="email" name='email' onChange={this.handleChange} placeholder="hello@" value={email} />
						<span className="icon is-small is-left">
							<i className="fas fa-envelope"></i>
						</span>
						<span className="icon is-small is-right">
							<i className="fas fa-exclamation-triangle"></i>
						</span>
					</div>
					{
						message && <p className={`help ${alert_color}`}>{message}</p>
					}
				</div>
				<div className="field">
					<div className="control">
						<label className="checkbox" onClick={this.handleAccept}>
							<input className={`is-medium`} type="checkbox" name='accept' value={accept} onChange={this.handleAccept} />
							<span className="checkbox-label">
								J’accepte que homejober stocke et traite mes données personnelles.
							</span>
						</label>
					</div>
				</div>
				<div className="field is-grouped">
					<div className="control">
						<button type='submit' className="button is-medium button-submit">Je m'inscris</button>
					</div>
				</div>
			</form>
		)
	}

}

export default BetaForm