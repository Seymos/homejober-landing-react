import React, { Component, Fragment } from "react";
import Navbar from "./../../components/Navbar";
import "./styles.scss";
import Modal from "../../components/Modal";
import logo_white from "../../assets/images/logo-homejober-white.png"
import logo_color from "../../assets/images/logo_homejober_color.png"
import smartphone from "../../assets/images/smartphone.png"

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalActive: false,
      isMobile: false
    }
  }

  componentDidMount() {
    this.checkDevice()
  }

  toggleModal = () => {
    this.setState({ modalActive: !this.state.modalActive })
  }

  checkDevice = () => {
    if (window.screen.width < 421) {
      this.setState({ isMobile: true })
    }
  }

  render() {
    const { modalActive, isMobile } = { ...this.state }
    return (
        <div className="hero is-fullheight">
          <div className="hero-head">
            <Navbar
                color="light"
                spaced={false}
                logo={isMobile ? logo_color : logo_white}
            />
          </div>
          <div className="hero-body">
            <section className="container">
              <div className="HomePage__columns columns">
                <div className="column">
                  {
                    modalActive ?
                        <Fragment>
                          <Modal closeModal={this.toggleModal}/>
                        </Fragment>
                        :
                        <Fragment>
                          <h1 className="hero-title">
                            Trouvez une mission ou un indépendant en un temps record !
                          </h1>
                          <p className="hero-p">
                            La plateforme Homejober vous permet de trouver le profil qui
                            correspond à 100% à votre besoin, synchronise vos agenda et vous
                            propose une mise en relation en temps réel.
                          </p>
                          <div className="buttons">
                            <button className="button is-medium beta-subscribe" onClick={this.toggleModal}>
                              S'inscrire à la bêta
                            </button>
                          </div>
                        </Fragment>
                  }
                </div>
                <div className="column has-text-right">
                  <img
                      className="hero-img"
                      alt="Smartphone"
                      src={smartphone}
                  />
                </div>
              </div>
            </section>
          </div>
        </div>
    );
  }
}

export default HomePage;
